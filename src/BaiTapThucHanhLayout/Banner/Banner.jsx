import React, { Component } from "react";
import "./Banner.css"

export default class Banner extends Component {
  render() {
    return (
      <div className="container d-flex px-lg-5">
        <div
          className="Banner">
          <h2 style={{ fontSize: "50px" }}>A warm welcome!</h2>
          <p>
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </p>
          <button className="btn btn-primary btn-lg" style={{ width: "20%" }}>
            Call to action
          </button>
        </div>
      </div>
    );
  }
}
