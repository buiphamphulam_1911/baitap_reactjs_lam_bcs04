import React, { Component } from 'react'
import Banner from './Banner/Banner'
import Body from './Body'
import Footer from './Foooter/Footer'
import Header from './Header/Header'
import Item from './Item'

export default class BaiThucHanhLayout extends Component {
  render() {
    return (
      <div>
        
        <Header/>
        <Body/>
        <Banner/>
        <Item/>
        <Footer/>
        
      </div>
    )
  }
}
