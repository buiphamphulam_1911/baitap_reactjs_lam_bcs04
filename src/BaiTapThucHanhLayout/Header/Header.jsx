import React, { Component } from "react";
import "./Header.css";

export default class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <div className="banner_1 container px-lg-5 ">
            <div className="logo">
              <a href="">Star Bootstrap</a>
            </div>
            <div className="navbar">
              <a href="" className="active">
                Home
              </a>
              <a href="">About</a>
              <a href="">Contact</a>
            </div>
          </div>
        </header>
      </div>
    );
  }
}
