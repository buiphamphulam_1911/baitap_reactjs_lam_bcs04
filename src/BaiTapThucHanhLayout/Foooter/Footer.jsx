import React, { Component } from "react";
import "./Footer.css";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer"
    
      >
        <p
          style={{
            color: "white",
            lineHeight: "100px",
            textAlign:"center"
          }}
        >
          Copyright © Your Website 2022
        </p>
      </div>
    );
  }
}
