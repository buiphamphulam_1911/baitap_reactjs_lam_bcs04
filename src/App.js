import logo from './logo.svg';
import './App.css';
import BaiThucHanhLayout from './BaiTapThucHanhLayout/BaiThucHanhLayout';

function App() {
  return (
    <div >
      <BaiThucHanhLayout/>
    </div>
  );
}

export default App;
